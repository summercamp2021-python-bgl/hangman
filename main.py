import random as rand

# Word list for hangman
with open("words.txt") as f:
    words = f.read().splitlines()

max_tries = input("Max tries for guessing (-1 = infinite)>> ")
try:
    max_tries = -1 if max_tries == "" else int(max_tries)
    pass
except ValueError as e:
    print("Please enter a number")
    exit(1)
    pass

word = rand.choice(words).upper()


uniqe_chars = []
for char in word:
    if not char in uniqe_chars:
        uniqe_chars.append(char)

tried_chars = []

while max_tries == -1 or max_tries > 0:
    if max_tries != -1:
        max_tries -=1
    print(''.join([('_' if not char in tried_chars else char) for char in word]),end="  ")
    char = input(f"Enter a character to guess (not {''.join(tried_chars)})").upper()
    if(len(char) != 1):
        print("Please enter only one char!")
        continue
    
    if char in uniqe_chars and not char in tried_chars:
        tried_chars.append(char)
        if(all(char in tried_chars for char in uniqe_chars)):
            break
        print("You guessed a correct char!")
        pass
    elif char in tried_chars:
        print("Alredy tried!")
        pass
    else:
        print("Not correct!")
        pass
    pass

if max_tries <= 0 and max_tries != -1:
    print("Too many tries")
else:
    print(f"\n\nYou guessed the word! The word was {word}")
 
